package com.test.adminarea;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FunctionRepository {
	
	//Testing code change
	//Test code 2
	
WebDriver driver;
	
	public void browserLaunch()
	{
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe"); //kept driver in same project
		driver = new ChromeDriver();
		
	}
	
	 public void maximizeBrowser()
	 {
		driver.manage().window().maximize(); 
		
	 }
	 public void adminStoreAppLaunch()
	 {
		 String URL="https://admin-demo.nopcommerce.com/login?ReturnUrl=%2Fadmin%2F";
		 driver.get(URL);
	 }
	 public void closeBrowser()
	 {
		 driver.close();
	 }
	 public void waitFor(int timespan) throws InterruptedException
	 {
		 Thread.sleep(timespan);
	 }
	 public void enterUserName(String uName)
	 {
		  
		 WebElement txtuname=driver.findElement(By.xpath("//input[@type='email']"));
		 txtuname.sendKeys(uName);
	 }
	 public void enterPassWord()
	 {
		 //WebElement txtpwd=driver.findElement(By.name("password"));
		 WebElement txtpwd=driver.findElement(By.xpath("//input[@name='Password']"));
		 txtpwd.sendKeys("admin");
	 }
	 public void signIn() throws AWTException
	 {
		 //WebElement signinbtn=driver.findElement(By.name("login"));
		 WebElement signinbtn=driver.findElement(By.xpath("//input[@class='button-1 login-button']"));
		 signinbtn.click();
		 
		 /*Using ROBOT class for signing in
		 Robot r1=new Robot();
		 r1.keyPress(KeyEvent.VK_ENTER);*/
		 
	 }
	 public void verifyValidLogin()
	 {
		 String expectedtitle="Dashboard / nopCommerce administration";
		 String actualtitle=driver.getTitle();  // Runtime title of the web page
		 System.out.println("Getting the title of webpage from selenium webdriver: "+actualtitle);
		 if(expectedtitle.equals(actualtitle))
		 {
			 System.out.println("Login successful");
		 }
		 else
			 System.out.println("Login unsuccessful");
	 }
	 
	 public void navigateCatalog()
	 {
		 WebElement product=driver.findElement(By.xpath("/html[1]/body[1]/div[3]/div[2]/div[1]/ul[1]/li[2]/a[1]"));
		 product.click();
	 }

}
