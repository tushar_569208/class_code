package com.test.adminarea;

import java.awt.AWTException;

public class Driver {

	public static void main(String[] args) throws InterruptedException, AWTException {
		// TODO Auto-generated method stub
		FunctionRepository fr=new FunctionRepository();
		fr.browserLaunch();
		fr.waitFor(2000);
		fr.maximizeBrowser();
		fr.adminStoreAppLaunch();
		fr.waitFor(4000);
		fr.enterUserName("admin@yourstore.com");
		fr.waitFor(4000);
		fr.enterPassWord();
		fr.signIn();
		fr.verifyValidLogin();
		fr.waitFor(4000);
		fr.navigateCatalog();
		

	}

}
